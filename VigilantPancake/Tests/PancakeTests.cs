using System;
using VigilantPancake;
using Xunit;

namespace Tests
{
    public class PancakeTests
    {
        [Fact]
        public void TestAddIngredientToPancakeWorks()
        {
            var pancake = new Pancake();
            var ingredientOne = new Ingredient("banaan");
            var ingredientTwo = new Ingredient("stroop");
            
            pancake.AddIngredient(ingredientOne);
            pancake.AddIngredient(ingredientTwo);

            Assert.Contains(ingredientOne, pancake.Ingredients);
            Assert.Contains(ingredientTwo, pancake.Ingredients);
        }

        [Fact]
        public void TestAddingTooManyIngredientsThrowsPancakeTooHeavyException()
        {
            var pancake = new Pancake();

            int count = 5;
            int added = 0;
            
            while (added < count)
            {
                pancake.AddIngredient(new Ingredient($"Ingredient {added + 1}"));
                added++;
            }

            Assert.ThrowsAny<PancakeTooHeavyException>(() => pancake.AddIngredient(new Ingredient("")));
        }
    }
}